function search()  {

    // get search term from the search box
    let searchTerm = document.getElementById("userInput").value;
    console.log("User wants to search for " + searchTerm);

    // connect to pokeAPI and hopefully get some data back
    const URL = "https://pokeapi.co/api/v2/pokemon/" + searchTerm
    console.log("connecting to: " + URL);


    var xhr = new XMLHttpRequest
    xhr.open('GET', URL);
    xhr.send(null);

    xhr.onreadystatechange = function () {
        console.log("coming back!");
        const DONE = 4; // readyState 4 means the request is done.
        const OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
          if (xhr.status === OK) {

            // show the pokemon card:
            var card = document.querySelector("div.card");
            card.style.display = "block";


            // get response and convert it to a JSON dictionary
            // xhr.responseText is a STRING
            // JSON.parse(...) converts the STRING to a DICTIONARY
            let response = JSON.parse(xhr.responseText);
            console.log(response); // 'This is the returned text.'

            let pokemonName = response["name"];
            let height = response["height"];
            let weight = response["weight"];
            var image = response["sprites"]["front_default"];

            console.log("Name: " + pokemonName)
            console.log("Height: " + height)
            console.log("Image: " + image)



            // get the first 4 moves for this pokemon & put it into the user interface
            // ----------------------
            let moves = response["moves"];
            console.log("pokemon has: " + moves.length + " moves");

            let html = "<h3> Moves </h3>"
            html += "<p>"
            for (let i = 0; i < 4; i++) {
              let moveName = moves[i]["move"]["name"];
              moveName = formatMove(moveName);

              html += moveName
              html += "<br>"
            }
            html += "</p>"
            document.getElementById("pokeMoves").innerHTML = html;


            // put this information into the UI
            // ----------------------

            // capitalize first letter in the pokemon's name
            pokemonName = pokemonName.charAt(0).toUpperCase() + pokemonName.slice(1)
            document.getElementById("pokeName").innerHTML = pokemonName;

            // add the height & weight to the UI
            document.getElementById("pokeInfo").innerHTML = "Height: " + height + " decimeters <br>";
            document.getElementById("pokeInfo").innerHTML += "Weight: " + weight + " hectograms";

            // show a picture of the pokemon
            document.getElementById("pokeImage").setAttribute("src", image);

            // clear the search box
            document.getElementById("userInput").value = ""

          } else {
            console.log('Error: ' + xhr.status); // An error occurred during the request.
          }
        }
    }

}

function formatMove(move) {
  // split the string
  // replace - with space
  // capitalize letters
  let words = move.split("-");
  for (let i = 0; i < words.length; i++) {
    // capitalize the first letter
    words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1)
  }

  // reassemble the string
  let formattedMove = "";
  for (let j = 0; j < words.length; j++) {
    formattedMove += words[j]

    // add a space between words
    if ((j+1) < words.length) {
      formattedMove += " "
    }
  }
  console.log("formatted move: " + formattedMove)
  return formattedMove;
}
